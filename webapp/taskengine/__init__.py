import schedule
import time
import os
from threading import Thread

def _scheduler_loop(sleep_interval):
    while True:
        schedule.run_pending()
        time.sleep(sleep_interval)

os.environ['__taskengine_started__']='true'
schedule.clear('taskengine')
scheduler_thread = Thread(name='SchedulerThread', target=_scheduler_loop, args=(10,))
scheduler_thread.deamon = True
scheduler_thread.start()

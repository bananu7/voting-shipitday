import schedule
from enum import Enum
from concurrent.futures import Executor
from concurrent.futures import ThreadPoolExecutor

main_executor = ThreadPoolExecutor(max_workers=2)

class SafeWrapper:
    def __init__(self, executor: Executor, job_func, args, kwargs):
        self.executor = executor
        self.job_func = job_func
        self.args = args
        self.kwargs = kwargs
        self.cancel = None

    def _safe_call(self):
        try:
            if self.args and self.kwargs:
                self.cancel = self.job_func(*self.args, **self.kwargs)
            elif self.args:
                self.cancel = self.job_func(*self.args)
            elif self.kwargs:
                self.cancel = self.job_func(**self.kwargs)
            else:
                self.cancel = self.job_func()
        except Exception as exc:
            print('Function {} generated exception {}'.format(job_func, exc))
        except:     
            print('Function {} generated unknown exception'.format(job_func, exc))

    def execute(self):
        if isinstance(self.cancel, schedule.CancelJob) or self.cancel is schedule.CancelJob:
            return self.cancel
        self.executor.submit(SafeWrapper._safe_call, self)


class Units(Enum):
    SECONDS = 'seconds'
    MINUTES = 'minutes'
    HOURS = 'hours' 
    DAYS = 'days'
    WEEKS = 'weeks'


def register(job_func, unit:Units, interval=1, at_time:str=None, args=None, kwargs=None)->schedule.Job:
    job = schedule.every(interval)
    job = job.tag('taskengine')
    job.unit = unit.value
    if at_time:
        job.at(at_time)
    wrapper = SafeWrapper(main_executor, job_func, args, kwargs)
    job.do(SafeWrapper.execute, wrapper)
    return job
from rest_framework import serializers
from voting import models
from rest_framework.fields import CurrentUserDefault

class Imageserializer(serializers.ModelSerializer):
    class Meta:
        fields = ('image',)
        model = models.ImageModel
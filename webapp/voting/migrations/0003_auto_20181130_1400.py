# Generated by Django 2.1.3 on 2018-11-30 13:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('voting', '0002_imagemodel_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='VoteModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
            ],
        ),
        migrations.RemoveField(
            model_name='imagemodel',
            name='score',
        ),
        migrations.AddField(
            model_name='votemodel',
            name='image',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='voting.ImageModel'),
        ),
        migrations.AddField(
            model_name='votemodel',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]

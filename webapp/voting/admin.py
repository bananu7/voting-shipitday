from django.contrib import admin
from voting.models  import *

admin.site.register(ImageModel)
admin.site.register(VoteModel)
admin.site.register(VotingSession)

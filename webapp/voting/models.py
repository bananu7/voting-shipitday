import calendar
import time
from django.db import models
from django.contrib.auth.models import User


class VotingSession(models.Model):
	session_start_date = models.DateTimeField(null=True)
	vote_active = models.BooleanField(default = True)

class ImageModel(models.Model):
    ts = int(round(time.time() * 1000))
    image = models.ImageField(null=True, upload_to='images/'+str(ts))
    voting_session = models.ForeignKey(VotingSession, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "Image " + self.image.name

    def get_score(self):
    	return VoteModel.objects.filter(image=self).count()

class VoteModel(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	date = models.DateTimeField()
	image = models.ForeignKey(ImageModel, on_delete=models.CASCADE)
	votingSession = models.ForeignKey(VotingSession, on_delete=models.CASCADE, null=True)

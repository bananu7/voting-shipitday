'use strict';

const e = React.createElement;

class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            enlarged: false
        };
        this.enlargeImage = this.enlargeImage.bind(this);
    }

    enlargeImage() {
        this.setState({
            enlarged: !this.state.enlarged
        })
    }

    render() {
        let className = "";
        if (this.state.enlarged)
            className += ' enlarged'

        return (
            <img className={className} src={this.props.uri} onClick={this.enlargeImage}/>
        )
    }
}

class VotingPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false
        };
        this.postVote = this.postVote.bind(this)
    }

    postVote() {
        let data = new FormData();
        data.append('csrfmiddlewaretoken', window.csrf_token);

        fetch('/vote/' + window.voting_session + '/' + this.props.id + '/', {
            method: 'POST',
            body: data,
            credentials: 'same-origin',
        }).then(() => {
            this.setState({
                success: true
            })
            location.reload()
        });
    }

    render() {
        if (this.state.success) {
            return (<span>success!</span>);
        } else {
            return (<input type="button" value="Vote!" onClick={this.postVote}/>);
        }
    }
}

class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            images: props.images,
            hasVoted: props.hasVoted
        }
    }

    OptionalVotingPanel(props) {
        if (! window.has_voted) {
            return <VotingPanel id={props.id} />
        } else {
            return (
                <dt>Score</dt>,
                <dd>{props.score}</dd>
            )
        }
        return null;
    }

    render() {
        let images = [];
        let i = 0;
        for (let i in this.state.images) {
            let img = this.state.images[i];
            images.push(
                <div className="image-container" key={img.id.toString()}>
                    <Image uri={"/media/" + img.url}/>
                    <dl>
                      <dt>Author:</dt>
                      <dd>{img.author}</dd>
                    </dl>
                    <this.OptionalVotingPanel id={img.id} score={img.score} />
                </div>
            );
        }

        return images;
    }
}

ReactDOM.render(<Gallery images={window.images}/>, document.getElementById('gallery'));
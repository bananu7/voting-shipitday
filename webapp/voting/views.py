from django.shortcuts import render
from django.views.generic import ListView, TemplateView
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status
from rest_framework.authtoken.models import Token

from voting.models import ImageModel, VoteModel, VotingSession
from voting.serializers import Imageserializer

from datetime import datetime

class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'voting/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        sessions = VotingSession.objects.filter(vote_active=True)
        context['sessions'] = sessions
        return context

class ImageList(LoginRequiredMixin, ListView):
    model = ImageModel
    context_object_name = 'images'

    def get_queryset(self, **kwargs):
        voting_session = VotingSession.objects.get(pk=self.kwargs['voting_session'])
        return ImageModel.objects.filter(voting_session=voting_session)

    def get_context_data(self, **kwargs):
        context = super(ImageList, self).get_context_data(**kwargs)
        voting_session = VotingSession.objects.get(pk=self.kwargs['voting_session'])

        has_voted = VoteModel.objects.filter(user=self.request.user, votingSession=voting_session).count() > 0

        context['user_has_voted'] = has_voted
        context['voting_session'] = voting_session.pk
        return context    

# TODO: make this more reasonable
def get_default_voting_session():
    return VotingSession.objects.filter(vote_active=True)[0]

class ImageUpload(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        image_serializer = Imageserializer(data=request.data)
        if image_serializer.is_valid():
            # image.voting_session = VotingSession.objects.filter(voting_active=True)[0]
            image = image_serializer.save()
            image.user = request.user
            image.voting_session = get_default_voting_session()
    
            if ImageModel.objects.filter(voting_session=image.voting_session, user=image.user).count() > 0:
                return Response('Image already uploaded by this user to this voting session',
                    status=status.HTTP_406_NOT_ACCEPTABLE)

            image.save()
            return Response("OK", status=status.HTTP_201_CREATED)
        else:
            return Response(image_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# TODO: add csrf token to fetch
class CastVoteView(LoginRequiredMixin, APIView):
    def post(self, request, *args, **kwargs):
        v = VoteModel()
        v.image = ImageModel.objects.get(pk=kwargs['id'])
        v.user = request.user
        v.date = datetime.now()
        v.votingSession = VotingSession.objects.get(pk=kwargs['voting_session'])
        v.save()
        return Response("Vote OK", status=status.HTTP_201_CREATED)

class SettingsView(LoginRequiredMixin, TemplateView):
    template_name='voting/settings.html'

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['token'] = Token.objects.get_or_create(user=self.request.user)[0]
        return context
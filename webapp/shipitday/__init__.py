import sys
import os
import time
from threading import Thread

def _init_tasks():
    time.sleep(5)
    from . import tasks

started = os.environ.get('__taskengine_started__')
if 'runserver' in sys.argv and not started:
    import taskengine
    scheduler_thread = Thread(name='InitTasks', target=_init_tasks)
    scheduler_thread.start()

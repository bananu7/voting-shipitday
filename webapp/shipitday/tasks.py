from taskengine.Engine import *
from voting.tasks import VotingEmail
from voting.tasks import DownloadEmail

register(unit = Units.DAYS, at_time='09:00', job_func = DownloadEmail.send_emails);
register(unit = Units.DAYS, at_time='10:00', job_func = VotingEmail.send_emails);

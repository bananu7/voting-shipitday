FROM python:3
ADD webapp /usr/src/webapp
WORKDIR /usr/src/webapp/
RUN pip install -r /usr/src/webapp/requirements.txt
CMD python ./manage.py  migrate --settings shipitday.settings_prod && python ./manage.py runserver --settings shipitday.settings_prod  0.0.0.0:80



﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.IO;
using System.Runtime.Caching;

namespace SaWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObjectCache cache = MemoryCache.Default;
        private static readonly HttpClient client = new HttpClient();
        private string imgUrlReg;
        private string imgFileName;

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                this.imgUrlReg = FindImage.FindCurrentImage();
                var splitted = imgUrlReg.Split('\\');
                this.imgFileName = splitted[splitted.Length - 1];
                var image = new BitmapImage();
                using (FileStream fs = new FileStream(imgUrlReg, FileMode.Open))
                {
                    image.BeginInit();
                    image.StreamSource = fs;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.EndInit();
                }
                background.Background = new ImageBrush(image);
            }
            catch (Exception ex)
            {
                communicationBox.Text = ex.Message;
            }
            
            serverBox.Text = SaWPF.Properties.Settings.Default.serverUrl;
            tokenBox.Text = SaWPF.Properties.Settings.Default.serverToken;
        }

        private async void sendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaWPF.Properties.Settings.Default.serverUrl = serverBox.Text;
                SaWPF.Properties.Settings.Default.serverToken = tokenBox.Text;
                SaWPF.Properties.Settings.Default.Save();

                var file = File.ReadAllBytes(imgUrlReg);

                var resp = await Upload(file, serverBox.Text, tokenBox.Text, this.imgFileName);
                communicationBox.Text = resp;
            }
            catch(Exception ex)
            {
                communicationBox.Text = ex.Message;
            }
        }

        public static async Task<string> Upload(byte[] image, string adress, string token, string imgFileName)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Token " + token);
                using (var content =
                    new MultipartFormDataContent())
                {
                    content.Add(new StreamContent(new MemoryStream(image)), "image", imgFileName + ".jpg");

                    using (
                       var message =
                           await client.PostAsync(adress, content))
                    {
                        var input = await message.Content.ReadAsStringAsync();

                        return input;
                    }
                }
            }
        }
    }
}
